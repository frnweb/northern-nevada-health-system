<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GocQUnah08UvBXlztH1fDSjbZFkzDPrv6AYXZqcW96xSIROUbm94coaViIlmg2UrR/40rafHOsPjQdRgVESulw==');
define('SECURE_AUTH_KEY',  'HQwm7hWDTGWcsDb9SHUbhM01ncQJ8Ewawy9ohMt927ethpZB7cfmLDAFKsF2ACJX8qmKFoK3f5Ix6FUy7/YsaQ==');
define('LOGGED_IN_KEY',    'dPWCHtmrc3XWqyWzaw/K4tLNNH5rrvJrU6+z6CLPRpmJ5aMFOb32S9Mgi8USMm2ZNxq4AIWbPsVJdDqN7P42lw==');
define('NONCE_KEY',        'yubDhThLCIV3sR7jLmJwougm5Jq0LEWQJiEpaIWNQaKGYwurdmirHiTnOyvlvjevUqPJFvtAwq785kjoOIrMPA==');
define('AUTH_SALT',        'oxlJKEwh6DMK3+fL8Xbr0Sxluqinbr16b7DilOjT5wNMYvQAIA0PU1rBsDMHAS9b1+Z7Yb+eB9v9Z2Y0Rt2S0g==');
define('SECURE_AUTH_SALT', 'nPS6I8HmKxWMZtnxpGVZnV4Us4kwOl/W2iclRMbToyNyXa2sVkvJwI5zR8PpcS0qKnwnpzjG8fSnXs5meU7cqw==');
define('LOGGED_IN_SALT',   'FHVr016pGKqm6WfUCeUoGwlAg61FB+L/ein+w8FysnljjTjrpyaKu7rklD6qDVMHuzqyJ9xbTnarh7ih+3aCLw==');
define('NONCE_SALT',       's9U+ghSPqrtt9aNKMWjEBpFzgCDZa4lufSXX356BNMaBwGCPIU/0cTVQWG0LboZ29VbEoI+qaU5nUtM15KIBNw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


// This enables debugging.
define( 'WP_DEBUG', true );



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
