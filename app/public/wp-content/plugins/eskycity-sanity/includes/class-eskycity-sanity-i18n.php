<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/includes
 * @author     eSkyCity <support@eskycity.com>
 */
class Eskycity_Sanity_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'eskycity-sanity',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
