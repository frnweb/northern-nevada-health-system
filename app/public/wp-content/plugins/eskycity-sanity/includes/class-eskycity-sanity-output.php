<?php

/**
 * Sanity Output Formatter
 *
 * This class is used generate GROQ queries from simple public methods, and provide access to the resulting query and JSON or scalar result.
 *
 * Also facilitates custom GROQ queries.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/includes
 * @author     eSkyCity <support@eskycity.com>
 */

require_once plugin_dir_path( __FILE__ ) . '../vendor/autoload.php';

use Sanity\BlockContent;

class Eskycity_Sanity_Output {

	private $page_size;
	private $use_autocomplete;

	public function __construct() {

		$this->page_size = intval( filter_var( get_option( 'eskycity_sanity_pagesize', FILTER_SANITIZE_NUMBER_INT ) ) );

		if ( $this->page_size === 0 ) {

			$this->page_size = 25;

		}

		$this->use_autocomplete = boolval( filter_var( get_option( 'eskycity_sanity_search_autocomplete' ), FILTER_SANITIZE_NUMBER_INT ) );
	}

	public function format_block_content( $html_block ) {

		$result = $html_block;

		if ( is_array( $result ) ) {

			//var_dump( $result );
			
			//$sanitized_result = array();
			
			//foreach ( $result as $key=>$value ) {
				
			//	$sanitized_result[$key] = $this->sanitize( $element . ".", Type::STRING );
			//}
			
			//var_dump( $sanitized_result );
			
			$result = BlockContent::toHtml( $html_block );
			//$result = BlockContent::toHtml( $sanitized_result );

		}

		return $result;

	}
	
	public function generate_pager ( int $page_size, int $page_count, int $current_page = NULL ) {

		$pager_html = "<div class='eskycity_sanity_pager'>";

		// Generate pager only if there's more than one page
		if ( $page_count > $page_size ) {

			// Get the current page URL
			$page_base = get_permalink( get_the_ID() );

			// Calculate total number of pages, rounding up
			$total_pages = ceil( $page_count / $page_size );
			
			$query_string = htmlspecialchars( $_SERVER['QUERY_STRING'], ENT_QUOTES );
			
			if ( ! empty( $query_string ) ) {
				
				$query_string = "?" . $query_string;
				
			}

			for ( $i = 1; $i <= $total_pages; $i++ ) {

				$page_url = $page_base;

				if ( $i > 1 ) {

					$page_url .= $i;

				}

				if ( $i != $current_page ) {

					$pager_html .= "<a class='eskycity_sanity_page' href='" . $page_url . $query_string . "'>" . $i . "</a> &nbsp; ";

				}
				else {

					$pager_html .= "<span class='eskycity_sanity_page_nolink'>" . $i . "</span> &nbsp; ";

				}

			}

		}

		$pager_html .= "</div>";

		return $pager_html;

	}
	
	public function get_pagesize() {

		return $this->page_size;

	}
	
	public function use_autocomplete() {

		return $this->use_autocomplete;

	}
	
	public function generate_autocomplete_options( string $field_id, array $variables ) {

		$option_list = "";

		foreach ( $variables as $i => $values ) {

			$option = '"';

			foreach ( $values as $key => $value ) {

				$option .= trim( $value ) . ' ';

			}

			$option = rtrim( $option, ' ' );

			$option_list .= $option . '", ';

		}

		$option_list = rtrim( $option_list, ', ' );

		$script = '<script>var options = [' . $option_list; 
		$script .= ']; autocomplete(document.getElementById("' . $field_id . '"), options);';
		$script .= '</script>';

		return $script;

	}
	
	public function get_currentpage( string $query_variable ) {

		$result = 1;

		$query_variable = trim( $query_variable );

		if ( get_query_var( $query_variable ) ) {

			$result = get_query_var( $query_variable );

		}

		return $result;

	}
	
	public function calculate_offset( int $current_page ) {

		$result = 0;

		if ( $current_page > 1 ) {

			$result = ( ( $current_page - 1 ) *  $this->page_size );

		}

		return $result;

	}
	
	public static function sanitize( $value, $type = NULL ) {
		
		$result = null;
		
		switch ( $type ) {
			
			case Type::STRING:
				
				$result = trim( sanitize_text_field( $value ) );
				break;
			
			case Type::INT:
				
				$result = filter_var( $value, FILTER_SANITIZE_NUMBER_INT );
				break;
		
			case Type::HTML:
				
				$result = esc_html( $value );
				break;
			
			case Type::URL:
				
				$result = esc_url_raw( $value );
				break;
			
			case Type::SLUG:
				
				$result = strtolower( trim( sanitize_html_class( $value ) ) );
				break;
			
			case Type::PHONE:
				
				$result = sanitize_key( $value );
				break;
			
			case Type::EMAIL:
				
				$result = sanitize_email( $value );
				break;
			
			case Type::BOOL:
				
				$result = filter_var( $value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE );
				break;
				
			default:
				
				$result = sanitize_text_field( $value );
				
		}
		
		return $result;
		
	}

}

abstract class Type {
	
	const STRING = 'string';
	const INT = 'int';
	const HTML = 'html';
	const URL = 'url';
	const SLUG = 'slug';
	const PHONE = 'phone';
	const EMAIL = 'email';
	const BOOL = 'bool';
	
}