<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.eskycity.com
 * @since      1.0.0
 *
 * @package    Eskycity_Sanity
 * @subpackage Eskycity_Sanity/admin/partials
 */

$options = Eskycity_Sanity_Options;

$active_tab = $options::get_options_tab();

?>

<div class="wrap">
	<h1><?php echo get_admin_page_title(); ?></h1>
	
	<h2 class="nav-tab-wrapper">
		<a class="<?php echo $options::get_options_class(); ?>" href="?page=eskycity-sanity">API</a>
		<a class="<?php echo $options::get_options_class('urls'); ?>" href="?page=eskycity-sanity&tab=urls">URLs</a>
		<a class="<?php echo $options::get_options_class('output'); ?>" href="?page=eskycity-sanity&tab=output">Output</a>
	</h2>
	
	<form method="post" action="options.php">
		<?php
		
		if ( $active_tab ==  'output') {
			
			// settings-page-output-group
			settings_fields( 'eskycity-sanity-settings-page-output-group' );
		
			// settings-page
			do_settings_sections( 'eskycity-santiy-output-page' );
			
			submit_button();
			
		}
		elseif ( $active_tab == 'urls' ) {
			
			// settings-page-options-group
			settings_fields( 'eskycity-sanity-settings-page-urls-group' );
			
			// settings-page
			do_settings_sections( 'eskycity-santiy-urls-page' );
			
			?>
			
			<div id="repeaterList" style="padding-bottom:10px;"></div>
			<button id="addItem" type="button">Add Rule</button>
		
			<script type="text/template" id="tmpl-repeater-field">
				<table class="form-table" role="presentation">
					<tbody>
						<tr><th scope="row">&nbsp;Rewrite Rule {{{ data.idvalue }}}</th><td></td></tr>
						<tr><td>Page Slug</td><td><input id="eskycity_sanity_urls[slug_{{{ data.idvalue }}}]" name="eskycity_sanity_urls[slug_{{{ data.idvalue }}}]" type="text" /></td></tr>
						<tr><td>Query Variable</td><td><input id="eskycity_sanity_urls[queryvar_{{{ data.idvalue }}}]" name="eskycity_sanity_urls[queryvar_{{{ data.idvalue }}}]" type="text" /></td></tr>
					</tbody>
				</table>
			</script>
		
		<?php

		submit_button( 'Save and Flush Permalinks' );
		
		}
		else {
			
			// settings-page-options-group
			settings_fields( 'eskycity-sanity-settings-page-options-group' );
			
			// settings-page
			do_settings_sections( 'eskycity-santiy-settings-page' );
			
			submit_button();
		}
		
		?>
	</form>
	
	
	<?php if ( $active_tab ==  null) { ?>
	
	<div id="sanity-check-loading" style="margin-bottom:10px; display:none;">
		<img src="images/wpspin_light.gif" />
		Loading...
	</div>
	
	<div id="sanity-check-result"></div>
	
	<form id="sanity-check" method="post" action="">
		<input id="sanity-check-button" type="submit" class="button" value="Test API Connection" />
	</form>
	
	<?php } ?>
	
	
</div>