(function( $ ) {
       'use strict';
  $(function(){
     $("#addItem").on( 'click', function(e){
       e.preventDefault();
       
       var txt = $('#itemInput').val();
       var template = wp.template('repeater-field');
       
       
       //var repeaterList = $('#repeaterList :input');
       var repeaterList = $('.form-table :input');
       var fieldSets = (repeaterList.length / 2);
       
       var html = template({ text: txt, idvalue: fieldSets});
       $("#repeaterList").append( html );
         
       //$("#repeaterList").append( 'Field Sets:' +  fieldSets);
     });
   });

})( jQuery );