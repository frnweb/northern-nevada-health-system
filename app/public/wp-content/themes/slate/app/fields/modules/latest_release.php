<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$latest_release = new FieldsBuilder('latest_release');

$latest_release 
	->addTab('settings', ['placement' => 'left'])
			->addFields(get_field_partial('partials.add_class'))
			->addFields(get_field_partial('partials.module_title'));

$latest_release
	->addTab('content', ['placement' => 'left'])
		//Header
		->addText('header', [
			'label' => 'Deck Header'
	    	])
	    	->setInstructions('This is optional')

	    //Module Button
	    ->addTrueFalse('module_button', [
			'wrapper' => ['width' => 30]
			])
			->setInstructions('Check to add a button to the bottom of the module')
		->addLink('button', [
			'wrapper' => ['width' => 70]
		])
		->conditional('module_button', '==', 1)

		//Type Select
		->addSelect('type_select', [
			'label' => 'Latest Type',
			'ui' => $config->ui,
		])
	  	->addChoices(
		  	['news' => 'News'],
        	['blog' => 'Blog']
      );
	  	
return $latest_release;