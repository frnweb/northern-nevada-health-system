<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],

];

$billboard = new FieldsBuilder('billboard');

$billboard
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));


$billboard
	
	->addTab('content', ['placement' => 'left'])
		->addGroup('billboard')

			// Header
			->addText('header', [
					'label' => 'Header',
					'ui' => $config->ui,
					'wrapper' => ['width' => 50]
				])
				->setInstructions('Header for the billboard')

			->addText('subheader', [
					'label' => 'Sub Header',
					'ui' => $config->ui,
					'wrapper' => ['width' => 50]
				])
				->setInstructions('Sub Header for the billboard. This field is optional')

			//Image 
			->addGroup('billboard_image')

				//Large Image 
				->addImage('large', ['wrapper' => ['width' => 50]])
					->setInstructions('Image background for billboard medium breakpoint and up')

				//Small Image 
				->addImage('small', ['wrapper' => ['width' => 50]])
					->setInstructions('Image background for billboard small breakpoint. This is optional')
				
			->endGroup()
			
			//Button
			->addFields(get_field_partial('modules.button'))
			
	   ->endGroup();

return $billboard;