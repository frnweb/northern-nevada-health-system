<?php

namespace App; 

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$faq = new FieldsBuilder('FAQ');

$faq
    ->addTab('settings', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.add_class'))
        ->addFields(get_field_partial('partials.module_title'));

$faq
    ->addTab('FAQ Type', ['placement' => 'left'])
    ->addSelect('type_select', [
        'label' => 'FAQ Type Select',
        'wrapper' => ['width' => 20]
    ])
    ->addChoices(
    ['simple' => 'Simple'],
    ['grouped' => 'Grouped']
    )

    ->addTab('content', ['placement' => 'left'])
      
	->addText('faq_header', [
		'label' => 'FAQ Header'
    ])
    
	// FAQ Relationship Field
    ->addRelationship('FAQ', [
        'label' => 'FAQs',
        'post_type' => 'sl_faqs_cpts',
        'ui' => $config->ui,
        ])
        ->conditional('type_select', '==', 'simple' )

    //Grouping Repeater
    ->addRepeater('accordion_group', [
      'min' => 1,
      'max' => 10,
      'button_label' => 'Add Grouping',
      'layout' => 'block',
    ])
        ->conditional('type_select', '==', 'grouped' )
        // Optional Icons
        ->addTrueFalse('add_icons', [
            'label' => 'Add icons to group',
        ])
        ->addImage('group_icon', [
            'label' => 'Group Icon',
            'ui' => $config->ui,
            'wrapper' => ['width' => 20],
        ])
        ->conditional('add_icons', '==', 1 )
        ->addText('group_title', [
            'label' => 'FAQ Group Title',
            'ui' => $config->ui,
            'wrapper' => ['width' => 80],
        ])
        // FAQ Relationship Field
        ->addRelationship('FAQ', [
            'label' => 'FAQs',
            'post_type' => 'sl_faqs_cpts',
            'ui' => $config->ui,
        ])
    ->endRepeater()

    // Optional Module Footer
    ->addTrueFalse('check_box', [
        'label' => 'Optional Module Footer',
    ])
        ->setInstructions('Adds a read all faq button and a callout to call our number')

    // Optional Conference Footer
    ->addTrueFalse('conf_check_box', [
        'label' => 'Optional Conference Footer',
    ])
    ->setInstructions('Adds a Foundations Events button and a callout to check out Foundations Events to learn more');
		
return $faq;