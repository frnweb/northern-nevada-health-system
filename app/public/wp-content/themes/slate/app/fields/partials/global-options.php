<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$optionsglobal = new FieldsBuilder('global_options');

$optionsglobal
    ->setLocation('options_page', '==', 'theme-general-settings');

$optionsglobal
    ->addTab('address', ['placement' => 'left'])
        //Address Fields
        ->addGroup('address', [
        ])      
            ->addText('location_name', [
                'label' => 'Location Name',
                'ui' => $config->ui
            ])
            ->setInstructions('Name of location or facility.')

            ->addText('street_address', [
                'label' => 'Street Address',
                'ui' => $config->ui
            ])
            ->setInstructions('The street address for the facility. This will be used throughout the Site.')

            ->addText('city', [
                'label' => 'City',
                'ui' => $config->ui
            ])
            ->setInstructions('Put the City i.e. Nashville')

            ->addText('state', [
                'label' => 'State',
                'ui' => $config->ui
            ])
            ->setInstructions('Put the State here i.e. TN')

            ->addText('zip_code', [
                'label' => 'Zip Code',
                'ui' => $config->ui
            ])
            ->setInstructions('The Zip Code of the facility')

            //Google Map
            ->addGoogleMap('google_map')

        ->endGroup();

$optionsglobal
    ->addTab('phone', ['placement' => 'left'])

        //Phone Numbers
        ->addGroup('phone', [
            'wrapper' => ['width' => 50]
        ])
            ->addText('main', [
                'label' => 'Global Phone Number',
                'ui' => $config->ui
            ])
            ->addText('sms', [
                'label' => 'SMS number',
                'ui' => $config->ui
            ])
        ->setInstructions('Add the number for the SMS messenging.')
        ->endGroup();

$optionsglobal
    ->addTab('sidebar', ['placement' => 'left'])

        //Sidebar Fields
        ->addRepeater('sidebar', [
          'min' => 0,
          'max' => 10,
          'label' => 'Global Sidebar',
          'instructions' => 'Add cards that will appear on the sidebar on every default page template',
          'button_label' => 'Add Sidebar Card',
          'layout' => 'block',
          'wrapper' => [
              'class' => 'deck',
            ],
        ])
            ->addGroup('item', [
                'label' => 'Sidebar Card',
        ])      
                ->addTrueFalse('link_wrapper', [
                    'wrapper' => ['width' => 30]
                    ])
                    ->setInstructions('Check to make whole card a clickable link')
                ->addLink('link_url', [
                    'wrapper' => ['width' => 70]
                ])
                ->conditional('link_wrapper', '==', 1)

                ->addText('title', [
                    'label' => 'Section Title',
                    'instructions' => 'Optional',
                ])

                // WYSIWYG
                ->addWysiwyg('content', [
                    'label' => 'WYSIWYG',
                    'instructions' => 'Optional',
                ])
            ->endGroup()
        ->endRepeater();
$optionsglobal
    ->addTab('Global CTA', ['placement' => 'left'])
        // WYSIWYG
        ->addWysiwyg('global_cta', [
            'label' => 'WYSIWYG',
            'instructions' => 'The call to action that appears in a callout at the bottom of most pages',
        ]);
$optionsglobal
->addTab('Sanity Query', ['placement' => 'left'])
    // Text
    ->addText('locationGrp_id', [
        'label' => 'Location Group ID',
        'instructions' => 'This is the ID for the _type LocationGrp',
    ]);
return $optionsglobal;