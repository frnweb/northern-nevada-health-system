<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 33],
];

$services = new FieldsBuilder('services_fields');

$services
    ->setLocation('post_type', '==', 'sl_services_cpts');
  
$services
	->addTab('callout', ['placement' => 'left'])
		->addGroup('callout', [
        ])      
		// WYSIWYG
		->addWysiwyg('content', [
			'label' => 'WYSIWYG',
			'instructions' => 'If not filled, it will fallback to what is added to the Global CTA in the Theme Settings',
			'ui' => $config->ui
		])
		->endGroup();
$services
	->addTab('locations', ['placement' => 'left'])
		->addGroup('locations', [
        ])      
		// WYSIWYG
		->addWysiwyg('content', [
			'label' => 'WYSIWYG',
			'instructions' => 'Optional intro paragraph',
			'ui' => $config->ui
		])
		->endGroup();

return $services;