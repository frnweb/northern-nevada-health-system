<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 33],
];

$locations = new FieldsBuilder('locations_fields');

$locations
    ->setLocation('post_type', '==', 'page')
    	->and('page_template', '==', 'template-location-group.php')
    	->or('page_template', '==', 'template-locations.php');
  
$locations
	->addTab('callout', ['placement' => 'left'])
		->addGroup('callout', [
        ])      
		// WYSIWYG
		->addWysiwyg('content', [
			'label' => 'WYSIWYG',
			'instructions' => 'If not filled, it will fallback to what is added to the Global CTA in the Theme Settings',
			'ui' => $config->ui
		])
		->endGroup();

return $locations;