jQuery(function($) {

    jQuery(document).ready(function() {

        $("#sl_input").on('keyup', function() {
            var input, filter, ul, li, a, i, dropdown, results, noResult;
            input = document.getElementById("sl_input");
            filter = input.value.toUpperCase();
            dropdown = document.getElementById("sl_dropdown");
            results = document.getElementById("sl_dropdown__results");
            a = results.getElementsByTagName("a");
            noResult = document.getElementById('sl_dropdown__noresults')

            dropdown.style.display = "block";

            for (i = 0; i < a.length; i++) {
                var txtValue = a[i].textContent || a[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    postTypeData.forEach(e => {
                        if(filter.toUpperCase() === e.post_title.toUpperCase()) {
                            window.location.replace("/" + e.slug);
                        }
                    });
                a[i].style.display = "";
                } else {
                a[i].style.display = "none";
                }
            }

            if($(results).children(':visible').length > 0) {
                $(noResult).hide();
            } else {
                $(noResult).show();
            }

            if(input.value === ""){
                dropdown.style.display = "none";
                $(noResult).hide();
            }
        })
    })
})