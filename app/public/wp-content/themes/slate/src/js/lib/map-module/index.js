const moment = require('moment'); // require

jQuery(function($) {
    jQuery( document ).ready(function() {
        //Sets toggle to show and hide map/card for mobile

        //CARD TOGGLE
        $(".sl_map__toggle__list").click(function() {
            $(".sl_map__toggle__list").addClass('sl_active');
            $(".sl_map__toggle__map").removeClass('sl_active'); 

            //List  
            $(".sl_map__map").hide();
            $(".sl_map__content").show();
        });

        //MAP TOGGLE
        $(".sl_map__toggle__map").click(function() {
            $(".sl_map__toggle__map").addClass('sl_active');
            $(".sl_map__toggle__list").removeClass('sl_active');  

            //DECK
            $(".sl_map__content").hide();
            $(".sl_map__map").show();
            map.setZoom(8);
        });
        
        // Sets the map center over Kansas City before markers load. 
        var latlng = new google.maps.LatLng(41.850033, -87.6500523);
        var mapOptions = {
        zoom: 8,
        center: latlng
        }
        
        // Initializes new map and passes it mapOptions credentials
        var map;

        var bounds = new google.maps.LatLngBounds();
        
        //Stores makersArray to be cleared on new search
        var markersArray = [];

        var mapMarkerArray = [];
        
        //Creates new marker window on click. 
        var infoWindow = new google.maps.InfoWindow;

        var element = document.getElementById('map_content');
        var count = document.getElementsByClassName('sl_map__count');

        // IE 10 or OLDER ERROR MESSAGE
        if(window.navigator.userAgent.indexOf('MSIE ') > 0) {
            $(".sl_facility__header").append('<div class="sl_ie-error"><p>You are using a browser that is not supported by the Google Maps JavaScript API. Consider changing your browser. <a href="https://browsehappy.com/">Upgrade your browser.</a></p></div>');
        }

        $("#sl_map__location").one("click", function() {

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
        
                if(confirm('Would you like to use your current location?')) {
        
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var searchData = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
        
                        var geocoder  = new google.maps.Geocoder();             // create a geocoder object
                        var location  = new google.maps.LatLng(searchData.lat, searchData.lng);    // turn coordinates into an object          
                        geocoder.geocode({'latLng': location}, function (results, status) {
                            var conceptName = $('#serviceTypeSelect').find(":selected").text();

                            if(status == google.maps.GeocoderStatus.OK) { 
                                var add = results[0].address_components[6].short_name;

                                clearOverlays();
                                $("#sl_map__zip").val(add);

                                // if geocode success
                                if (conceptName === 'All Services' || conceptName === 'Select a Service') {
                                    calculateDistances(add, myFacilityData);
                                } else if (conceptName !== 'All Services' || conceptName !== 'Select a Service') {
                                    var newArray = [];
                                    myFacilityData.forEach(e => {  
                                        e.serviceLine.forEach(f => {
                                            if( f.name.includes(conceptName) ){
                                                newArray.push(e)
                                            }
                                        })
                                    })
                                    calculateDistances(add, newArray);
                                }
                            }
                        });
        
                        var icon = {
                            url: '/wp-content/themes/slate/dist/img/icon/map-pin--primary.svg',
                            scaledSize: new google.maps.Size(30,45)
                        }
        
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(searchData.lat, searchData.lng),
                            animation: google.maps.Animation.DROP,
                            icon: icon,
                            map: map
                        });
                    
                        marker.addListener('click', function() {
                            infoWindow.setContent("Your current location")
                            infoWindow.open(map, marker);
                        });
        
                    });
                }
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }
                
            function handleLocationError(browserHasGeolocation, infoWindow, searchData) {
                infoWindow.setPosition(searchData);
                infoWindow.setContent(browserHasGeolocation ?
                                    'Error: The Geolocation service failed.' :
                                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }
        
        });

        //When search button is clicked, grabs value and passes it to the Distance calculator function
        $("#sl_map__button").click(function() {
            var searchData = $("#sl_map__zip").val();
            var conceptName = $('#serviceTypeSelect').find(":selected").text();


            if (searchData.length !== 0 && searchData.length !== 5 ) {
                alert("Please Enter a 5 Digit Zip Code")
            } else {
                if (searchData.length === 0) {

                } else if (conceptName === 'All Services' || conceptName === 'Select a Service') {
                    clearOverlays();
                    calculateDistances(searchData, myFacilityData);
                } else if (conceptName !== 'All Services' || conceptName !== 'Select a Service') {

                    var newArray = [];
                    myFacilityData.forEach(e => {  
                        e.serviceLine.forEach(f => {
                            if( f.name.includes(conceptName) ){
                                newArray.push(e)
                            }
                        })
                    })
                    clearOverlays();
                    if (newArray.length === 0){

                    } else {
                        calculateDistances(searchData, newArray);
                    }
                }
            }
        });

        //runs click button on enter keypress
        $("#sl_map__zip").keydown(function(e){
            if(e.which == 13){
                $("#sl_map__button").click();
            }
        });

        //Check if myFacilityData is an object that exists on window load, grabs markers and markup for all facilities from myFacilityData object passed from twig template, loads map.
        if (window.myFacilityData) {
            map = new google.maps.Map(document.getElementById('sl_map'), mapOptions);
            var myFacilityGrp = [];
            var myNewFacilityGrp = [];

            myFacilityData.forEach(e => {
                markersArray.push(e);
                addMarkup(e)
            })

            getMarkers(markersArray);
            map.fitBounds(bounds)

            $(count).text('Showing ' + $(element).children().length + ' Facilities');
 
        }

        //when tab is changed and search field is empty, it will generate markers/cards based on selected tab.
        $('#serviceTypeSelect').change(function() {

            var conceptName = $('#serviceTypeSelect').find(":selected").text();
            var searchData = $("#sl_map__zip").val();

            clearOverlays();

            if( (conceptName === 'All Services' || conceptName === 'Select a Service') ) {
                if (searchData.length === 0) {
                    myFacilityData.forEach(e => {   
                        markersArray.push(e);
                        addMarkup(e);
                    })
                    getMarkers(markersArray);
                    map.fitBounds(bounds)
                } else {
                    calculateDistances(searchData, myFacilityData);
                }
            } else if ( (conceptName !== 'All Services'|| conceptName !== 'Select a Service') ) {
                if (searchData.length === 0) {
                    myFacilityData.forEach(e => {   
                        e.serviceLine.forEach(f => {
                            if( f.name.includes(conceptName) ){
                                    markersArray.push(e);
                                    addMarkup(e)
                                }
                        })
                    })
                    getMarkers(markersArray);
                    map.fitBounds(bounds)
                } else {
                    var newArray = [];

                    myFacilityData.forEach(e => {
                        e.serviceLine.forEach(f => {
                            if( f.name.includes(conceptName) ){
                                newArray.push(e)
                            }
                        })
                    })

                    if (newArray.length === 0){

                    } else {
                        calculateDistances(searchData, newArray);
                    }
                }
            } 

            $(count).text('Showing ' + $(element).children().length + ' Facilities');

        });

        //Function iterates over myFacilityData objects and creates markup for cards. Appends html tags to div container and adds new element to slick. 
        function addMarkup(location){
            var name = location.facilityDistance ? '<div class="sl_card__header"><h3>'+ location.facilityName + '</h3><p class="sl_card__distance">' + location.facilityDistance + '</p></div>' : '<div class="sl_card__header"><h3>'+ location.facilityName + '</h3></div>'
            var city = location.facilityCity ? location.facilityCity + ', ' : ' ';

            if(location.facilityName === "ER at McCarran NW" && myERTimes !== null) 
            { 
                
                var m = moment.utc(Math.ceil(myERTimes[0].waitTimeInSeconds / 60))
                var erVisit = '<div class="sl_card__waittime"><p> ER Wait Time: ' + m + ' min.</p></div>'
            } else if (location.facilityName === "Northern Nevada Medical Center" && myERTimes !== null ) {
                var m = moment.utc(Math.ceil(myERTimes[1].waitTimeInSeconds / 60))
                var erVisit = '<div class="sl_card__waittime"><p> ER Wait Time: ' + m  + ' min.</p></div>'
            } else {
                var erVisit = ''
            }

            var address = '<div class="sl_card__address">' + erVisit + '<br>' + location.facilityAddress + '<br>' + city + location.facilityState + ' ' + location.facilityZip + '</div>'

            var image = location.facilityImage ? '<div class="sl_card__image" style="background-image: url(' + location.facilityImage + ')"></div>' : '<div class="sl_card__image" style="background-image: url(/wp-content/themes/slate/dist/img/location-thumbnail-fallback.jpg)"></div>';

            var anchor = location.facilityAnchor ? '<a class="sl_card__facility" href="/locations/' + location.facilityAnchor + '">Facility Information</a>' : '';

            var phone = location.facilityPhone ? '<a class="sl_card__phone" href="tel:' + location.facilityPhone + '">' + location.facilityPhone + '</a>' : '';

            var directions = location.facilityDir ? '<a class="sl_card__directions" href="' + location.facilityDir + '">Get Directions</a>' : '';

            var site = location.facilitySite ? '<a class="sl_card__website" href="' + location.facilitySite + '">Visit Website</a>' : '';

            var services = '<div class="sl_row small-up-2">';

            location.serviceLine.forEach( item => {
                services += '<div class="sl_cell"><a href="/services/' + item.slug + '">' + item.name +'</a></div>';
            });


            var templateString = '<div class="sl_card sl_card--map sl_map__card">' + name + '<div class="sl_card__location"><div class="sl_row">' + address + image + '</div>' + anchor + '</div><div class="sl_card__links">' + phone + directions + site + '</div><ul class="sl_card__services accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true"><li class="accordion-item" data-accordion-item><a href="#" class="accordion-title">Services</a><div class="accordion-content" data-tab-content>' + services + '</div></div></li></ul></div>'; 

            //Content
            $(element).append(templateString);

            reInit();

        }

        //Function iterates over myFacilityData objects and creates markers based on Lat/Lng. Also sets Marker card content, and adds event listener. setContent only accepts string, not object, can't append.
        function getMarkers(myFacilityData){

            const result=[]

            myFacilityData.forEach((obj,index)=>{
                result.push(
                    myFacilityData.filter(function(e) {
                        return obj.facilityAddress === e.facilityAddress
                    })
                )
            })

            let newResult = Array.from(new Set(result.map(JSON.stringify)), JSON.parse);

            newResult.forEach(myFacilityData => {

                if (myFacilityData.length === 1) {
                    var contentString = '<div class="sl_card sl_card--pin">' +
                    '<p>' + myFacilityData[0].facilityName + '</p>' +
                    '<a href="/locations/' + myFacilityData[0].facilityAnchor + '/' + '"">Facility Information</a></div>'
            
                    var icon = {
                        url: '/wp-content/themes/slate/dist/img/icon/map-pin--primary.svg',
                        scaledSize: new google.maps.Size(30,45)
                    }
                
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(myFacilityData[0].facilityLat, myFacilityData[0].facilityLng),
                        animation: google.maps.Animation.DROP,
                        icon: icon,
                        map: map,
                    });

                    bounds.extend(marker.position)

                    marker.addListener('click', function() {
                        infoWindow.setContent(contentString)
                        infoWindow.setOptions({
                        })
                        infoWindow.open(map, marker);
                    });
    
                    mapMarkerArray.push(marker);
                } else {

                    var facilities = ' ';

                    myFacilityData.forEach(function(item, index, array) {

                        if (index !== array.length -1 ) {
                            facilities += ('<p>' + item.facilityName + '</p>');
                        } else {
                            facilities += ('<p>' + item.facilityName + '</p>' +
                            '<a href="/locations/' + item.facilityAnchor + '/' + '"">Facility Information</a>');
                        }
                    });

                    var contentString = '<div class="sl_card sl_card--pin">' + facilities + '</div>'
            
                    var icon = {
                        url: '/wp-content/themes/slate/dist/img/icon/map-pin--multiple.svg',
                        scaledSize: new google.maps.Size(30,45)
                    }
                
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(myFacilityData[0].facilityLat, myFacilityData[0].facilityLng),
                        animation: google.maps.Animation.DROP,
                        icon: icon,
                        map: map,
                    });

                    bounds.extend(marker.position)
    
                    marker.addListener('click', function() {
                        infoWindow.setContent(contentString)
                        infoWindow.setOptions({
                        })
                        infoWindow.open(map, marker);
                    });
    
                    mapMarkerArray.push(marker);
                }
            })
                    
        }
    
        //Called to clear all markers from array
        function clearOverlays() {
            $(element).empty();
            for (var i = 0; i < mapMarkerArray.length; i++ ) {
                mapMarkerArray[i].setMap(null);
            }
            mapMarkerArray.length = 0;
            markersArray.length = 0;
        }

        //Filter function based on search value above, grabs all facility Zip Codes and used DistanceMatrix API to return travel distances. Calls calcDistance fucntion after return.
        function calculateDistances(searchData,facilities) {

            var service = new google.maps.DistanceMatrixService();
            
            var origin = searchData;

            facilities.forEach(facility => {
                service.getDistanceMatrix(
                    {
                        origins: [origin],
                        destinations: [facility.facilityZip],
                        travelMode: google.maps.TravelMode.DRIVING,
                        unitSystem: google.maps.UnitSystem.IMPERIAL,
                        avoidHighways: false,
                        avoidTolls: false
                    }, 
                    calcDistance(facility)
                )
            });
        }

        //takes the returned Distances object and only adds cards markup/markers to facilities and the correct dropdown type.
        function calcDistance(facility) {
            return function(response, status) {
                if (status != google.maps.DistanceMatrixStatus.OK) {
                    alert('Error was: ' + status);
                } else {
                    var origins = response.originAddresses;
                    var destinations = response;

                    var newFacilities = []

                    for (var i = 0; i < origins.length; i++) {
                        var results = response.rows[i].elements;

                        for (var j = 0; j < results.length; j++) {
                            //changes distance to integer, and removes comma, and "miles"
                            if (results[j].status != "ZERO_RESULTS"){
                                var distanceMiles = parseInt(results[j].distance.text.replace(/[^0-9]/g, ''));
                            }
                            
                            if (results[j].distance.text ==="1 ft") {
                                var distance = "00.0 mi"
                            } else {
                                var distance = results[j].distance.text;
                            }

                            newFacilities.push({...facility, "facilityDistance": distance})
                        }
                    }

                    newFacilities.sort((a, b) => (parseInt(a.facilityDistance) > parseInt(b.facilityDistance)) ? 1 : -1)

                    newFacilities.forEach( e => {
                        markersArray.push(e);
                        addMarkup(e);
                    })

                    getMarkers(markersArray);
                    map.fitBounds(bounds);
                    $(count).text('Showing ' + $(element).children().length + ' Facilities');
                }
            }
        }
    }) 

    function reInit(){
        var accordion = document.getElementsByClassName('sl_card__services');

        $(accordion).foundation();
    }
    
});