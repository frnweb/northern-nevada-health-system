<?php
/**
 * Template Name: Location Group
 * Description: Page template for location groups from Sanity
 */

use Sanity\BlockContent;

$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

$today = date('Ymd');
$todayTime = date('Y-m-d H:i:s');

//If eSky Sanity Plugin is activate expose logic and apply location to context variable in twig 
if( is_plugin_active( 'eskycity-sanity/eskycity-sanity.php' ) ) {
    $sanity_connector = new Eskycity_Sanity_Connector();
    $sanity_connector->sanity_connect();
    $sanity_query = new Eskycity_Sanity_Query( $sanity_connector );
    $output = new Eskycity_Sanity_Output();
    $sanity_search = new Eskycity_Sanity_Search();

    //Groq query specifically for locations within location group
    $fields = array (
        "'name': name", 
        "'locations': { 
            'ind': location[]->{
                ...,
                services[]{...,
                    serviceLine->,
                    levelCare->
                },
                'imageURL': select(
                    gallery[] != null => gallery[0].asset->url),
                          'logoURL' : select(
                        logo_horizontal != null => logo_horizontal.asset->url,
                    ),
            }, 
            'parallel': parallelGrp[]->{
                'name':name,
                'anchor': anchor,
                'locations':location[]->{
                    ...,
                    services[]{...,
                        serviceLine->,
                        levelCare->
                    },
                    'imageURL': select(
                    gallery[] != null => gallery[0].asset->url),
                          'logoURL' : select(
                        logo_horizontal != null => logo_horizontal.asset->url,
                    ),
                }
            } 
        }", 
    );
    
    //grabs the Location Group ID string from Theme General Settings
    $queryoptions = get_fields('options');
    $searches = array("_id == \"". $queryoptions['locationGrp_id'] ."\"");

    //Applies the Groq query and specific Location Group ID string to the eSky Sanity query. Returns results as unfiltered locations.
    $sanity_query->fetch('locationGrp', $fields, $searches, 0, 1);
    $unfil_locations = $sanity_query->get_result();

    $locations = array();

    //if an unfil_locations exists, sort locations that are individual or grouped and put them in one single array. 
    if(isset($unfil_locations)) {
        foreach($unfil_locations as $val) {
            foreach($val['locations']['ind'] as $ind ){
        
                $serviceLine = array();
                foreach($ind['services'] as $service){
                    array_push($serviceLine, (object) [
                        'name' => $service['serviceLine']['name'],
                        'slug' => isset($service['serviceLine']['anchor']) ? $service['serviceLine']['anchor'] : '',
                    ]);
                }
        
                $newInd = (object) array(
                    "facilityName" => isset($ind['name']) ? $ind['name'] : '',
                    "facilityHours" => isset($ind['hrs_247']) ? $ind['hrs_247'] : '',
                    "facilityImage" => isset($ind['imageURL']) ? $ind['imageURL'] : '',
                    "facilityLogo" => isset($ind['logoURL']) ? $ind['logoURL'] : '',
                    "facilitySite" => isset($ind['link']['home']) ? $ind['link']['home'] : '',
                    "facilityPage" => isset($ind['slug']['current']) ? $ind['slug']['current'] : '',
                    "facilityAnchor" => isset($ind['anchor']) ? $ind['anchor'] : '',
                    "facilityLat" => isset($ind['geopoint']['lat']) ? $ind['geopoint']['lat'] : '',
                    "facilityLng" => isset($ind['geopoint']['lng']) ? $ind['geopoint']['lng'] : '',
                    "facilityAddress" => isset($ind['addr']['line1']) ? $ind['addr']['line1'] : '',
                    "facilityCity" => isset($ind['addr']['city']) ? $ind['addr']['city'] : '',
                    "facilityState" => isset($ind['addr']['state']) ? strtoupper($ind['addr']['state']) : '',
                    "facilityZip" => isset($ind['addr']['code']) ? $ind['addr']['code'] : '',
                    "facilityDir" =>isset($ind['addr']['city']) ? str_replace(' ', '+', "https://www.google.com/maps/dir//".$ind['addr']['line1'].",+".$ind['addr']['city'].",+".$ind['addr']['state']."+".$ind['addr']['code']."/@".$ind['geopoint']['lat'].",".$ind['geopoint']['lng'].",17z/") : '',
                    "serviceLine" => $serviceLine,
                    "entryType" => "Individual",
                    "facilityPhone" => isset($ind['telNum']['main']) ? $ind['telNum']['main'] : '',
                    "facilityDesc" => isset($ind['descLong']) ? BlockContent::toHtml($ind['descLong']) : '',
                );
        
                if( $post->slug == $newInd->facilityAnchor) {
                    array_push($locations, $newInd);
                }
            }
            foreach($val['locations']['parallel'] as $parallel ){
        
                foreach($parallel['locations'] as $location ){
                    $serviceLine = array();
        
                    if(isset($location['services'])) {
                        foreach($location['services'] as $service){
                            array_push($serviceLine, (object) [
                                'name' => $service['serviceLine']['name'],
                                'slug' => isset($service['serviceLine']['anchor']) ? $service['serviceLine']['anchor'] : '',
                            ]);
                        }
                    }
        
                    $newParallel = (object) array(
                        "locationGrp" => isset($parallel['name']) ? $parallel['name'] : '',
                        "facilityName" => isset($location['name']) ? $location['name'] : '',
                        "facilityHours" => isset($location['hrs_247']) ? $location['hrs_247'] : '',
                        "facilityImage" => isset($location['imageURL']) ? $location['imageURL'] : '',
                        "facilityLogo" => isset($location['logoURL']) ? $location['logoURL'] : '',
                        "facilitySite" => isset($location['link']['home']) ? $location['link']['home'] : '',
                        "facilityPage" => isset($location['anchor']) ? $location['anchor'] : '',
                        "facilityAnchor" => isset($parallel['anchor']) ? $parallel['anchor'] : '',
                        "facilityLat" => isset($location['geopoint']['lat']) ? $location['geopoint']['lat'] : '',
                        "facilityLng" => isset($location['geopoint']['lng']) ? $location['geopoint']['lng'] : '',
                        "facilityAddress" => isset($location['addr']['line1']) ? $location['addr']['line1'] : '',
                        "facilityCity" => isset($location['addr']['city']) ? $location['addr']['city'] : '',
                        "facilityState" => isset($location['addr']['state']) ? strtoupper($location['addr']['state']) : '',
                        "facilityZip" => isset($location['addr']['code']) ? $location['addr']['code'] : '',
                        "facilityDir" => str_replace(' ', '+', "https://www.google.com/maps/dir//".$location['addr']['line1'].",+".$location['addr']['city'].",+".$location['addr']['state']."+".$location['addr']['code']."/@".$location['geopoint']['lat'].",".$location['geopoint']['lng'].",17z/"),
                        "serviceLine" => $serviceLine,
                        "entryType" => "Grouping",
                        "facilityPhone" => isset($location['telNum']['main']) ? $location['telNum']['main'] : '',
                        "facilityDesc" => isset($location['descLong']) ? BlockContent::toHtml($location['descLong']) : '',
                    );
        
                    if( $post->slug == $newParallel->facilityAnchor) {
                        array_push($locations, $newParallel);
                    }
                }
            }
        }
    }

    //Apply sorted locations global context variable for twig. 
    $context['sanity_locations'] = $locations;

    $context['json_locations'] = json_encode($locations);

}

Timber::render( array( 'templates/location-group.twig', 'page.twig' ), $context );
