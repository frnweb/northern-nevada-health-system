<?php
/**
 * Template Name: Landing Page
 * Description: Page template for any top level page. Has access to slate modules.
 */
use Sanity\BlockContent;

$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

$today = date('Ymd');
$todayTime = date('Y-m-d H:i:s');

//GRABS MOST RECENT POSTS
$articleargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '4', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date'
    );
$context['posts'] = Timber::get_posts( $articleargs );

//GRABS MOST RECENT BLOG POSTS
$blogargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '3', // Number of posts
    'category__not_in'  => '6',// EXCLUDE NEWS CATEGORY ID
    'order'          => 'DESC',
    'orderby'        => 'date'
    );
$context['blog'] = Timber::get_posts( $blogargs );

//GRABS MOST RECENT NEWS POSTS
$blogargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '3', // Number of posts
    'category__in'  => '6', // INCLUDE NEWS CATEGORY ID
    'order'          => 'DESC',
    'orderby'        => 'date'
    );
$context['news'] = Timber::get_posts( $blogargs );


$post_type_field = get_field('modules');

//Checks if modules have been added to page. 
if(!empty($post_type_field)){
    foreach($post_type_field as $val) {
        $posts = array();
        if ($val['acf_fc_layout'] == 'post_type_search'){
            $post_type = $val['post_type'];
        
            // //GRABS MOST RECENT POSTS
            $searchargs = array(
                'post_type'      => $post_type,
                'posts_per_page' => '-1', // Number of posts (no limit)
                'order'          => 'ASC',
                'orderby'        => 'title'
                );
        
            $post_type = Timber::get_posts( $searchargs );
    
            foreach($post_type as $post){
                $newPost = (object) array(
                    "post_title" => isset($post->post_title) ? $post -> post_title : '',
                    "slug" => isset($post -> slug) ? $post->slug : '',
                );
    
                array_push($posts, $newPost);
            }
    
            $context['post_type'] = $posts;
    
            $context['json_post_type'] = json_encode($posts);
        
        }
    }
    
    foreach($post_type_field as $val) {
        
        // //GRABS MOST RECENT POSTS
        $searchargs = array(
            'post_type'      => 'sl_services_cpts',
            'posts_per_page' => '-1', // Number of posts (no limit)
            'order'          => 'ASC',
            'orderby'        => 'title'
            );
    
        $context['services'] = Timber::get_posts( $searchargs );
    }
}


//If eSky Sanity Plugin is activate expose logic and apply location to context variable in twig 
if( is_plugin_active( 'eskycity-sanity/eskycity-sanity.php' ) ) {
    $sanity_connector = new Eskycity_Sanity_Connector();
    $sanity_connector->sanity_connect();
    $sanity_query = new Eskycity_Sanity_Query( $sanity_connector );
    $output = new Eskycity_Sanity_Output();
    $sanity_search = new Eskycity_Sanity_Search();

    //Groq query specifically for locations within location group
    $fields = array (
        "'name': name", 
        "'locations': { 
            'ind': location[]->{
                ...,
                services[]{...,
                    serviceLine->,
                    levelCare->
                },
                'imageURL': select(
                    gallery[] != null => gallery[0].asset->url),
                          'logoURL' : select(
                        logo_horizontal != null => logo_horizontal.asset->url,
                    ),
            }, 
            'parallel': parallelGrp[]->{
                'name':name,
                'anchor': anchor,
                'locations':location[]->{
                    ...,
                    services[]{...,
                        serviceLine->,
                        levelCare->
                    },
                    'imageURL': select(
                    gallery[] != null => gallery[0].asset->url),
                          'logoURL' : select(
                        logo_horizontal != null => logo_horizontal.asset->url,
                    ),
                }
            } 
        }", 
    );
    
    //grabs the Location Group ID string from Theme General Settings
    $queryoptions = get_fields('options');
    $searches = array("_id == \"". $queryoptions['locationGrp_id'] ."\"");

    //Applies the Groq query and specific Location Group ID string to the eSky Sanity query. Returns results as unfiltered locations.
    $sanity_query->fetch('locationGrp', $fields, $searches, 0, 1);
    $unfil_locations = $sanity_query->get_result();

    $locations = array();

    //if an unfil_locations exists, sort locations that are individual or grouped and put them in one single array. 
    if(isset($unfil_locations)) {
        foreach($unfil_locations as $val) {
            foreach($val['locations']['ind'] as $ind ){
    
                $serviceLine = array();
                if(isset($ind['services'])) {
                    foreach($ind['services'] as $service){
                        array_push($serviceLine, (object) [
                            'name' => $service['serviceLine']['name'],
                            'slug' => isset($service['serviceLine']['anchor']) ? $service['serviceLine']['anchor'] : '',
                        ]);
                    }
                }
    
                $newInd = (object) array(
                    "facilityName" => isset($ind['name']) ? $ind['name'] : '',
                    "facilityHours" => isset($ind['hrs_247']) ? $ind['hrs_247'] : '',
                    "facilityImage" => isset($ind['imageURL']) ? $ind['imageURL'] : '',
                    "facilityLogo" => isset($ind['logoURL']) ? $ind['logoURL'] : '',
                    "facilitySite" => isset($ind['link']['home']) ? $ind['link']['home'] : '',
                    "facilityPage" => isset($ind['slug']['current']) ? $ind['slug']['current'] : '',
                    "facilityAnchor" => isset($ind['anchor']) ? $ind['anchor'] : '',
                    "facilityLat" => isset($ind['geopoint']['lat']) ? $ind['geopoint']['lat'] : '',
                    "facilityLng" => isset($ind['geopoint']['lng']) ? $ind['geopoint']['lng'] : '',
                    "facilityAddress" => isset($ind['addr']['line1']) ? $ind['addr']['line1'] : '',
                    "facilityCity" => isset($ind['addr']['city']) ? $ind['addr']['city'] : '',
                    "facilityState" => isset($ind['addr']['state']) ? strtoupper($ind['addr']['state']) : '',
                    "facilityZip" => isset($ind['addr']['code']) ? $ind['addr']['code'] : '',
                    "facilityDir" =>isset($ind['addr']['city']) ? str_replace(' ', '+', "https://www.google.com/maps/dir//".$ind['addr']['line1'].",+".$ind['addr']['city'].",+".$ind['addr']['state']."+".$ind['addr']['code']."/@".$ind['geopoint']['lat'].",".$ind['geopoint']['lng'].",17z/") : '',
                    "serviceLine" => $serviceLine,
                    "entryType" => "Individual",
                    "facilityPhone" => isset($ind['telNum']['main']) ? $ind['telNum']['main'] : '',
                    "facilityDesc" => isset($ind['descLong']) ? BlockContent::toHtml($ind['descLong']) : '',
                );
    
                array_push($locations, $newInd);
            }
            foreach($val['locations']['parallel'] as $parallel ){
    
                foreach($parallel['locations'] as $location ){
                    $serviceLine = array();
    
                    if(isset($location['services'])) {
                        foreach($location['services'] as $service){
                            array_push($serviceLine, (object) [
                                'name' => $service['serviceLine']['name'],
                                'slug' => isset($service['serviceLine']['anchor']) ? $service['serviceLine']['anchor'] : '',
                            ]);
                        }
                    }
    
                    $newParallel = (object) array(
                        "locationGrp" => isset($parallel['name']) ? $parallel['name'] : '',
                        "facilityName" => isset($location['name']) ? $location['name'] : '',
                        "facilityHours" => isset($location['hrs_247']) ? $location['hrs_247'] : '',
                        "facilityImage" => isset($location['imageURL']) ? $location['imageURL'] : '',
                        "facilityLogo" => isset($location['logoURL']) ? $location['logoURL'] : '',
                        "facilitySite" => isset($location['link']['home']) ? $location['link']['home'] : '',
                        "facilityPage" => isset($location['anchor']) ? $location['anchor'] : '',
                        "facilityAnchor" => isset($parallel['anchor']) ? $parallel['anchor'] : '',
                        "facilityLat" => isset($location['geopoint']['lat']) ? $location['geopoint']['lat'] : '',
                        "facilityLng" => isset($location['geopoint']['lng']) ? $location['geopoint']['lng'] : '',
                        "facilityAddress" => isset($location['addr']['line1']) ? $location['addr']['line1'] : '',
                        "facilityCity" => isset($location['addr']['city']) ? $location['addr']['city'] : '',
                        "facilityState" => isset($location['addr']['state']) ? strtoupper($location['addr']['state']) : '',
                        "facilityZip" => isset($location['addr']['code']) ? $location['addr']['code'] : '',
                        "facilityDir" => str_replace(' ', '+', "https://www.google.com/maps/dir//".$location['addr']['line1'].",+".$location['addr']['city'].",+".$location['addr']['state']."+".$location['addr']['code']."/@".$location['geopoint']['lat'].",".$location['geopoint']['lng'].",17z/"),
                        "serviceLine" => $serviceLine,
                        "entryType" => "Grouping",
                        "facilityPhone" => isset($location['telNum']['main']) ? $location['telNum']['main'] : '',
                        "facilityDesc" => isset($location['descLong']) ? BlockContent::toHtml($location['descLong']) : '',
                    );
    
                    array_push($locations, $newParallel);
                }
            }
        }
    }

    //Apply sorted locations global context variable for twig. 
    $context['sanity_locations'] = $locations;

    $context['json_locations'] = json_encode($locations);

}


    //ER Wait Time logic.
    //Now piggy backs off of ER plugin. Passes REST API with facility codes Theme Options and makes request. Returned response is applied to global Twig context.
    $er_fields = get_fields('er_fields');

    $er_codes = [];

    if(isset($er_fields)) {
        if ($er_fields['facility_list']['facility_repeater']) {

            $repeater = $er_fields['facility_list']['facility_repeater'];
        
            foreach ($repeater as $fields) {
                if ($fields['facility_code'] !== ''){
                    array_push($er_codes, $fields['facility_code']);
                }
            } 
        
            $list = implode(',', $er_codes);
    
            if ($list !== ''){
                $erurl = 'https://api.uhsinc.com/erwaittime/api/waittime/?faccodes='.$list;
    
                $opts = array("http" =>
                array("header" => 'Authorization: Bearer LH6d4kj2L7alooOCw4w65UXuZ5rtZFIdZPaYe6Cw6ybBqL-2HzDc14KD56qyUvy9JzDK_Kb9BK1EPS7JkhassoEEolwf4tMOvjVOn_vSFlTwIk6_rheYVPMmfa0ek1CvNiu47ShmdKocxn4LMcgjZ2COh_9nls28ceWHPvdV4P77avb2jfph-3R1HB7vIUKt1CXLQZIpg4JpYivCyVgHEmnfuFxANJwPqvsQIG6DSOVQdTC9AGYXYqy-uKVIuKrLE3UjDVAKK--1Wz3rAHH34gD-yM8')
                );
        
                $ercontext = stream_context_create($opts);
            
                $response = file_get_contents($erurl, false, $ercontext);
    
                $context['er_waittimes'] = $response;
            }
            
        };
    }

Timber::render( array( 'templates/landing-page.twig', 'page.twig' ), $context );


