<?php
/*
  Plugin Name: Slate Custom Post Types & Shortcodes
  Description: Adding functionality for custom post types (staff, events, and testimonials) and shortcodes (buttons, callouts).
  Version: 0.1
  Author: Slate
*/
require_once( 'cpts/services.php' );

/* SAMPLE CUSTOM POST TYPE */
require_once( 'shortcodes/button.php' );
require_once( 'shortcodes/callout.php' );
require_once( 'shortcodes/callout-card.php' );
require_once( 'shortcodes/container.php' );
require_once( 'shortcodes/gallery.php' );
require_once( 'shortcodes/grid.php' );
require_once( 'shortcodes/blockgrid.php' );
require_once( 'shortcodes/divider.php' );
require_once( 'shortcodes/accordion.php' );
require_once( 'shortcodes/tabs.php' );
require_once( 'shortcodes/thumbnail-card.php' );
require_once( 'shortcodes/sharethis.php' );
require_once( 'shortcodes/tooltip.php' );
require_once( 'shortcodes/embed.php' );
require_once( 'shortcodes/video-modal.php' );
require_once( 'shortcodes/read-next.php' );
require_once( 'shortcodes/settings.php' );
?>