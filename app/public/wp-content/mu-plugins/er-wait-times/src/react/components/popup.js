import React from 'react'

import moment from 'moment'

export const PopUp = ( props ) => {

    function waitTimeConversion(seconds){
        var min = Math.ceil(moment.duration(seconds * 1000).asMinutes());
        return min
    }

  return (
    <div id="er_popup" className="er_popup" data-toggler=".er_active">
        <div className="er_popup__open" data-toggle="er_popup">
            <div className="er_popup__icon">
                <i className="fal fa-stopwatch"></i>
            </div>
            <div className="er_popup__title">
                <h3>View ER Wait Times</h3>
            </div>
        </div>
        <div className="er_popup__card">
            <div className="er_popup__header">
                <h3>ER Wait Time</h3><a title="close-popup" className="er_popup__close" data-toggle="er_popup"><i className="fal fa-times-circle"></i></a>
            </div>
            <div className="er_popup__er">
                {data.map(((item, i) => (
                    <div className="er_popup__facility" key={i}>
                            <a className="er_popup__time" href={item.facility_url}>{waitTimeConversion(item.waitTimeInSeconds)} Min.</a> 
                            <div className="er_popup__name">
                                <p>{item.facility_title}</p>
                                <a href={item.facility_url}>View Map</a>
                            </div>
                    </div>
                )))}
            </div>
            <div className="er_popup__content">
                <p>If you are having a medical emergency, call 9-1-1. Refresh your screen to get the latest wait time. For the most accurate wait time, call the hospital.</p>
            </div>
        </div>
    </div>
  )
}

export default PopUp
