<?php
/**
 * Plugin Name: Services CPTS Plugin
 * Description: This is the Custom Post Type for Services.
 * Author: M.M.
 * License: GPL2
*/

// Register Custom Post Type
function sl_services_cpts() {

	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'sl_services_cpts' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'sl_services_cpts' ),
		'menu_name'             => __( 'Services', 'sl_services_cpts' ),
		'name_admin_bar'        => __( 'Services', 'sl_services_cpts' ),
		'archives'              => __( 'Services Archives', 'sl_services_cpts' ),
		'attributes'            => __( 'Services Attributes', 'sl_services_cpts' ),
		'parent_item_colon'     => __( 'Parent Service:', 'sl_services_cpts' ),
		'all_items'             => __( 'All Services', 'sl_services_cpts' ),
		'add_new_item'          => __( 'Add New Service', 'sl_services_cpts' ),
		'add_new'               => __( 'Add New', 'sl_services_cpts' ),
		'new_item'              => __( 'New Service', 'sl_services_cpts' ),
		'edit_item'             => __( 'Edit Service', 'sl_services_cpts' ),
		'update_item'           => __( 'Update Service', 'sl_services_cpts' ),
		'view_item'             => __( 'View Service', 'sl_services_cpts' ),
		'view_items'            => __( 'View Services', 'sl_services_cpts' ),
		'search_items'          => __( 'Search Services', 'sl_services_cpts' ),
		'not_found'             => __( 'Not found', 'sl_services_cpts' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sl_services_cpts' ),
		'featured_image'        => __( 'Featured Image', 'sl_services_cpts' ),
		'set_featured_image'    => __( 'Set featured image', 'sl_services_cpts' ),
		'remove_featured_image' => __( 'Remove featured image', 'sl_services_cpts' ),
		'use_featured_image'    => __( 'Use as featured image', 'sl_services_cpts' ),
		'insert_into_item'      => __( 'Insert into Service', 'sl_services_cpts' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Service', 'sl_services_cpts' ),
		'items_list'            => __( 'Services list', 'sl_services_cpts' ),
		'items_list_navigation' => __( 'Services list navigation', 'sl_services_cpts' ),
		'filter_items_list'     => __( 'Filter Services list', 'sl_services_cpts' ),
	);
	$args = array(
		'label'                 => __( 'Services', 'sl_services_cpts' ),
		'description'           => __( 'Custom Post Type for Services', 'sl_services_cpts' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'rewrite' => array('slug' => 'services','with_front' => false),
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-heart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sl_services_cpts', $args );

}
add_action( 'init', 'sl_services_cpts', 0 );
?>